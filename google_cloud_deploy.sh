PROJECT_ID=$(gcloud config get-value project)
gcloud services enable cloudbuild.googleapis.com --project=$PROJECT_ID

gcloud app deploy --project $PROJECT_ID --quiet
