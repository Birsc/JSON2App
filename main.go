package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/polyapp-open-source/polyapp/actions"
	"gitlab.com/polyapp-open-source/polyapp/allDB"
	"gitlab.com/polyapp-open-source/polyapp/common"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8081"
	}
	http.HandleFunc("/", JSON2App)

	// TLSPath is used for local development on SSL.
	TLSPath, TLSPathExists := os.LookupEnv("TLSPATH")
	if !TLSPathExists {
		log.Fatal(http.ListenAndServe(":"+port, nil))
	} else {
		cert := TLSPath + string(os.PathSeparator) + "server.crt"
		key := TLSPath + string(os.PathSeparator) + "server.key"
		log.Fatal(http.ListenAndServeTLS(":"+port, cert, key, nil))
	}
}

// JSON2App takes an actions.HTTPSCallRequest serialized into JSON; converts the data to Polyapp’s Edit Task format;
// generates the Task, Schema, and UX; and redirects the browser to a page where you can create a new Data.
func JSON2App(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("ioutil.ReadAll: %v", err.Error()), 500)
		return
	}
	var HTTPSCallRequest actions.HTTPSCallRequest
	HTTPSCallResponse := actions.HTTPSCallResponse{
		Request:     &common.POSTRequest{},
		Response:    &common.POSTResponse{},
		CreateDatas: []common.Data{},
		UpdateDatas: []common.Data{},
		DeleteDatas: []string{},
	}
	err = json.Unmarshal(body, &HTTPSCallRequest)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Unmarshal: %v", err.Error()), 500)
		return
	}
	err = jSON2App(&HTTPSCallRequest, &HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("jSON2App: %v", err.Error()), 500)
		return
	}
	responseBytes, err := json.Marshal(HTTPSCallResponse)
	if err != nil {
		http.Error(w, fmt.Sprintf("json.Marshal: %v", err.Error()), 500)
		return
	}
	_, err = w.Write(responseBytes)
	if err != nil {
		http.Error(w, fmt.Sprintf("w.Write: %v", err.Error()), 500)
		return
	}
}

type s struct {
	Name string
	JSON string `suffix:"JSON"`
	Link string
}

// jSON2App converts the data in the request to Polyapp’s Edit Task format;
// generates the Task, Schema, and UX; and redirects the browser to a page where you can create a new Data.
func jSON2App(request *actions.HTTPSCallRequest, response *actions.HTTPSCallResponse) error {
	var err error
	data := common.Data{}
	err = data.Init(request.CurrentData)
	if err != nil {
		return fmt.Errorf("data.Init: %w", err)
	}
	data.FirestoreID = request.Request.DataID
	S := s{}
	err = common.DataIntoStructure(&data, &S)
	if err != nil {
		return fmt.Errorf("DataIntoStructure: %w", err)
	}
	if S.Name == "" {
		return errors.New("S.Name was empty")
	}
	if S.JSON == "" {
		return errors.New("S.JSON was empty")
	}
	if S.Link != "" {
		// Do not run if Link is set.
		return nil
	}

	// convert the data in the request to Polyapp’s Edit Task format
	providedJSONMap := make(map[string]interface{})
	err = json.Unmarshal([]byte(S.JSON), &providedJSONMap)
	if err != nil {
		return fmt.Errorf("json.Unmarshal: %w", err)
	}

	providedJSONMap = fixInvalidKeys(providedJSONMap)

	datas, dependencyTree, err := actions.CreateEditTaskFromJSON("polyapp", "JSON2App Apps", S.Name, providedJSONMap)
	if err != nil {
		return fmt.Errorf("actions.CreateEditTaskFromJSON: %w", err)
	}

	// generate the Task, Schema, and UX
	errChan := make(chan error)
	for i := range datas {
		go func(d common.Data, errChan chan error) {
			err = allDB.CreateData(&d)
			if err != nil {
				errChan <- fmt.Errorf("allDB.CreateData: %w", err)
				return
			}
			errChan <- nil
		}(datas[i], errChan)
	}
	combinedErr := ""
	for range datas {
		err := <-errChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	if len(combinedErr) > 0 {
		return errors.New("combinedErr from storing Datas: " + combinedErr)
	}

	// Depth first search the tree and construct the dependencies first, then update the parents. Repeat for entire tree.
	var headTask common.Data
	for i := range datas {
		if datas[i].FirestoreID == dependencyTree.FirestoreID {
			headTask = datas[i]
			break
		}
	}
	err = createTaskRecursively(dependencyTree, datas, request.Request.UserID, request.Request.RoleID)
	if err != nil {
		return fmt.Errorf("createTaskRecursively: %w", err)
	}
	schemaID := datas[0].S[common.AddFieldPrefix(datas[0].IndustryID, datas[0].DomainID, datas[0].SchemaID, "Schema ID")]
	if schemaID == nil {
		return errors.New("Head Schema did not have an ID in its Edit Task Data")
	}
	headSchema, err := allDB.ReadSchema(*schemaID)
	if err != nil {
		return fmt.Errorf("allDB.ReadSchema %v: %w", schemaID, err)
	}

	allData, err := actions.ImportArbitraryJSONRecursively(providedJSONMap, headSchema)
	if err != nil {
		return fmt.Errorf("ImportArbitraryJSONRecursively: %w", err)
	}
	errChan = make(chan error)
	for i := range allData {
		go func(d *common.Data, errChan chan error) {
			err := allDB.CreateData(d)
			if err != nil {
				errChan <- fmt.Errorf("allDB.CreateData %v: %w", d.FirestoreID, err)
				return
			}
			errChan <- nil
		}(allData[i], errChan)
	}
	combinedErr = ""
	for range allData {
		err := <-errChan
		if err != nil {
			combinedErr += err.Error() + "; "
		}
	}
	if len(combinedErr) > 0 {
		return fmt.Errorf("errors creating data: %v", combinedErr)
	}

	// redirect the browser to a page where you can create a new Data.
	S.Link = common.CreateURL(headTask.IndustryID, headTask.DomainID,
		*headTask.S[common.AddFieldPrefix(headTask.IndustryID, headTask.DomainID, headTask.SchemaID, "Task ID")],
		*headTask.S[common.AddFieldPrefix(headTask.IndustryID, headTask.DomainID, headTask.SchemaID, "UX ID")],
		*headTask.S[common.AddFieldPrefix(headTask.IndustryID, headTask.DomainID, headTask.SchemaID, "Schema ID")],
		"", "", "", "", "")
	response.Response.NewURL = S.Link

	err = common.StructureIntoData(data.IndustryID, data.DomainID, data.SchemaID, &S, &data)
	if err != nil {
		return fmt.Errorf("StructureIntoData: %w", err)
	}
	err = allDB.UpdateData(&data)
	if err != nil {
		return fmt.Errorf("allDB.updateData: %w", err)
	}

	return nil
}

// createTaskRecursively checks if a Task has dependencies. If it does, it calls createTaskRecursively.
func createTaskRecursively(dependencyTree *actions.Tree, datas []common.Data, userID string, roleID string) error {
	var err error
	for i := range dependencyTree.Children {
		err = createTaskRecursively(dependencyTree.Children[i], datas, userID, roleID)
		if err != nil {
			return fmt.Errorf("createTaskRecursively: %w", err)
		}
	}
	var headTask *common.Data
	for i := range datas {
		if datas[i].FirestoreID == dependencyTree.FirestoreID {
			headTask = &datas[i]
			break
		}
	}
	if headTask == nil {
		return errors.New("headTask was nil for FirestoreID: " + dependencyTree.FirestoreID)
	}

	// update this Data's subtasks with the Task, Schema, and UX IDs from its children.
	for dependencyTreeChildrenIndex, child := range dependencyTree.Children {
		// there is a 1:1 between dependencyTree.Children and Subtasks
		childTaskID := ""
		childSchemaID := ""
		childUXID := ""
		for i := range datas {
			if datas[i].FirestoreID == child.FirestoreID {
				// This is the Subtask's "Edit Task" Data.
				childTaskID = *datas[i].S[common.AddFieldPrefix(datas[i].IndustryID, datas[i].DomainID, datas[i].SchemaID, "Task ID")]
				childSchemaID = *datas[i].S[common.AddFieldPrefix(datas[i].IndustryID, datas[i].DomainID, datas[i].SchemaID, "Schema ID")]
				childUXID = *datas[i].S[common.AddFieldPrefix(datas[i].IndustryID, datas[i].DomainID, datas[i].SchemaID, "UX ID")]
				break
			}
		}
		if childTaskID == "" || childSchemaID == "" || childUXID == "" {
			return errors.New("a dependencyTree.Children was not found in datas")
		}

		// we assume the order of Subtasks == the order of dependencyTree.Children
		subtasks := headTask.ARef["polyapp_TaskSchemaUX_SYfKUkHjtbesOmcBsPVyimJjk_Subtasks"]
		if len(subtasks) != len(dependencyTree.Children) {
			return errors.New("len(subtasks) != len(dependencyTree.Children) at ID: " + child.FirestoreID)
		}
		subtaskRef, err := common.ParseRef(subtasks[dependencyTreeChildrenIndex])
		if err != nil {
			return fmt.Errorf("common.ParseRef: %w", err)
		}

		// We now know the ID of the Subtask. We could read it from the database, but instead we'll use our convenient
		// cache of common.Data stored in datas.
		var updatedSubtask common.Data
		subtaskIndexInDatas := 0
		for i := range datas {
			if datas[i].FirestoreID == subtaskRef.DataID {
				updatedSubtask = datas[i]
				subtaskIndexInDatas = i
				break
			}
		}
		if updatedSubtask.FirestoreID == "" {
			return errors.New("subtask was not found in datas: " + subtaskRef.DataID)
		}
		updatedSubtask.S[common.AddFieldPrefix(updatedSubtask.IndustryID, updatedSubtask.DomainID, updatedSubtask.SchemaID, "Task ID")] = common.String(childTaskID)
		updatedSubtask.S[common.AddFieldPrefix(updatedSubtask.IndustryID, updatedSubtask.DomainID, updatedSubtask.SchemaID, "Schema ID")] = common.String(childSchemaID)
		updatedSubtask.S[common.AddFieldPrefix(updatedSubtask.IndustryID, updatedSubtask.DomainID, updatedSubtask.SchemaID, "UX ID")] = common.String(childUXID)
		err = allDB.UpdateData(&updatedSubtask)
		if err != nil {
			return fmt.Errorf("allDB.UpdateData: %w", err)
		}
		// Must update datas in addition to the database.
		datas[subtaskIndexInDatas] = updatedSubtask
	}
	err = actions.ActionEditTask(headTask, &common.POSTRequest{
		IndustryID: "polyapp",
		DomainID:   "TaskSchemaUX",
		UserID:     userID,
		RoleID:     roleID,
	})
	if err != nil {
		return fmt.Errorf("actions.ActionEditTask: %w", err)
	}
	for i := range datas {
		if datas[i].FirestoreID == dependencyTree.FirestoreID && headTask != nil {
			datas[i] = *headTask
			break
		}
	}
	return nil
}

// createDatasRecursively checks if a Data has childre. If it does, it calls createDatasRecursively.
func createDatasRecursively(dataMap map[string]interface{}, dependencyTree *actions.Tree, datas []common.Data, userID string, roleID string) error {
	var err error
	var editTaskData common.Data
	for i := range datas {
		if datas[i].FirestoreID == dependencyTree.FirestoreID {
			editTaskData = datas[i]
			break
		}
	}
	editTask := actions.EditTask{}
	err = common.DataIntoStructure(&editTaskData, &editTask)
	if err != nil {
		return fmt.Errorf("common.DataIntoStructure: %w", err)
	}
	for k := range dataMap {
		dataMap[common.AddFieldPrefix(editTask.Industry, editTask.Domain, editTask.SchemaID, k)] = dataMap[k]
		delete(dataMap, k)
	}
	d := common.Data{}
	// Init ignores maps, which is super convenient for us. It means we can do our own manual handling of Ref and ARef.
	d.Init(dataMap)
	d.IndustryID = editTask.Industry
	d.DomainID = editTask.Domain
	d.SchemaID = editTask.SchemaID
	err = allDB.CreateData(&d)
	if err != nil {
		return fmt.Errorf("allDB.CreateData: %w", err)
	}
	return nil
}

// fixInvalidKeys handles invalid key names. We shouldn't rely upon people to paste in valid key names, and we should
// correct invalid key names for them. This function does that for you.
func fixInvalidKeys(providedJSONMap map[string]interface{}) map[string]interface{} {
	fixedMap := make(map[string]interface{})
	for k := range providedJSONMap {
		switch v := providedJSONMap[k].(type) {
		case map[string]interface{}:
			fixedMap[fixOneKey(k)] = fixInvalidKeys(v)
		case []map[string]interface{}:
			fixedMap[fixOneKey(k)] = make([]interface{}, len(v))
			for i := range v {
				fixedMap[fixOneKey(k)].([]interface{})[i] = fixInvalidKeys(v[i])
			}
		case []interface{}:
			m := make([]map[string]interface{}, len(v))
			var ok bool
			for i := range v {
				m[i], ok = v[i].(map[string]interface{})
				if !ok {
					break
				}
			}
			if !ok {
				fixedMap[fixOneKey(k)] = providedJSONMap[k]
				continue
			}
			fixedMap[fixOneKey(k)] = make([]interface{}, len(m))
			for i := range m {
				fixedMap[fixOneKey(k)].([]interface{})[i] = fixInvalidKeys(m[i])
			}
		default:
			fixedMap[fixOneKey(k)] = v
		}
	}
	return fixedMap
}

func fixOneKey(s string) string {
	return strings.ReplaceAll(s, "_", " ")
}
