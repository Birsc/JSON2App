# JSON2App
[Convert arbitrarily complex JSON to a form based App here](https://polyapp-public.appspot.com/t/polyapp/JSON2App/pgEkUqNXuBBumtcETUsrkIdcg?ux=rOsRiwokAzbhtaQweVefLsNGV&schema=syugoFwCVGANyYpiVERflsdqn&data=new)

Demo video: https://youtu.be/qVsMif4dlm4

[Import more data here (you'll need to know your Schema's Name)](https://polyapp-public.appspot.com/t/polyapp/Import/NmWRIUMwmZsZTPkqtBiacDVjE?ux=kgGbDPiPAmGvJawTgbfjyXzUS&schema=FLDIivYCRvcKHvSJkPQMMVwTy&data=new)

[Export data here (you'll need to know your Schema's Name)](https://polyapp-public.appspot.com/t/polyapp/Export/fKgfaVERIycoCdJoaXmhDTSYr?ux=ZHrpqruzyePaJZLMXbGqiNNzg&schema=uGhKgwWImpQhdXIPssYLfuxaO&data=new)

## Use Cases
Easy-Edit Utility: Edit JSON field values or field names with this program. Export the resulting JSON.

Less Code App Development: Generate a form based front end for your app. Re-generate the front end or use "Edit Task" to adjust the form as you make changes.

## Editing generated Apps
You edit the generated app with Polyapp. There are 4 basic pages in Polyapp: Home, Change Domain, Change Data, and a plethora of Tasks which can be opened with Data.
For example, the "JSON2App" page you load, or the "Import" page or the "Export" page which are linked on this README are all Tasks. The links provided in this README open the Task with a new Data (see &data=new in the link URLs).
When you click on "Change Data", you see all "Data" associated with a particular Task. There are buttons to create a new one (which is similar to opening the Task with &data=new) and to "Edit Task".

To edit a Task, you need to get to its "Change Data" page and then click "Edit Task". This brings you to the "Edit Task" Task with the Data which is associated with this Task (I know this sentence is confusing).

From the "Edit Task" Task you can edit the Task and do things like add fields, rename fields, reorder them, change the UI element for a field, etc. When you click "Done" the Task is re-created based on what you specified. Now if you open up a Data for that Task, you'll see the UI has updated to match your changes.

Polyapp can run arbitrary code when a Task is Done. That is how JSON2App works. When you click "Done" it makes an API call to JSON2App's URL and Polyapp waits for the response. JSON2App does its thing and then Polyapp's execution resumes.

## Limitations
1. The ordering of the JSON fields is not preserved. Instead, the fields are ordered alphabetically.

2. JSON2App does not handle Schemas which are at the same JSON level but contained in different JSON objects. In other words...

This is fine: ```{"array":[{"a":"b"},{"a":"b","b":"c"}]}``` as it creates a Task with fields "a" and "b" and references this Task as an array of subtasks called "array".

This is not fine: ```{"hours": [{"sun": [{"closed": true}]}, {"mon": [{"open": "00:00","close": "17:00"}]} ] }``` 

Because it will create 2 different Tasks which have the fields "open" and "close" instead of referencing the same 1 Task from 2 different parent Tasks. 

In more detail: This is not fine because it creates a Task with field "hours" which is an array of subtasks; 
the "hours" Task will have subtasks named "sun", "mon", etc.; 
and each of those Tasks will have an array of subtasks with fields "open" and "closed". 
Critically, the subtasks with fields "open" and "closed" will be *different* and *independent*. 
After generating this app, you could use "Edit Task" to delete mon's subtask and replace it with sun's subtask, thereby fixing this issue.

The penalty for not doing this is similar to the penalty to copy-pasting code instead of making a function called from both locations. It's not an immediate issue, but it increases maintenance costs.

## Notes

This project depends on Polyapp: https://gitlab.com/polyapp-open-source/polyapp

This repository takes JSON and converts it to Polyapp’s Edit Task format; generates the Task, Schema, and UX; creates a new Data using the input JSON’s field values; and redirects the browser to view that Data.
